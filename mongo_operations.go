
package database

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"
	"bytes"
	"html/template"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gitlab.com/equeduct-webapp/backend125/email"
	"equeduct.com/module/data"
)

func InsertMongo(formDetails data.UserDetails) {

	hostName := os.Getenv("MONGO_DB_HOST")
	dbName := os.Getenv("MONGO_DB_NAME")
	user := os.Getenv("MONGO_USER")
	pass := os.Getenv("MONGO_PASS")

	if dbName == "" || user == "" || pass == "" {
		fmt.Println("mongo credentials not set")
		return
	}
	uri := `mongodb+srv://` + user + `:` + pass + `@` + hostName +`/` + dbName + `?retryWrites=true&w=majority`

	fmt.Println("Trying to connect mongodb server...")

	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	
	if err != nil {
		fmt.Println("Could Not connect to mongo server")
		log.Fatal(err)
	}

	ctx := context.Background()
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Starting mongodb data saving...")

	// connecting to DB and collections
	db := client.Database("equeduct")
	usersCollection := db.Collection("tbl_users")
	companiesCollection := db.Collection("tbl_companies")
	bankAccountsCollection := db.Collection("tbl_bank_accounts")
	userIdentificationsCollection := db.Collection("tbl_user_identifications")

	// Insert One
	result, err := usersCollection.InsertOne(ctx, formDetails.User)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Stored User in MondoDB")

	//objectID := fmt.Sprintf("%v", result.InsertedID)
	objectID := result.InsertedID.(primitive.ObjectID).Hex()
	formDetails.Companies.UID = objectID
	formDetails.BankAccounts.UID = objectID
	formDetails.UserIdentifications.UID = objectID

	_, err = companiesCollection.InsertOne(ctx, formDetails.Companies)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Stored Company in MondoDB")

	_, err = bankAccountsCollection.InsertOne(ctx, formDetails.BankAccounts)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Stored BankAccounts in MondoDB")

	formDetails.IdentificationFrontSide = formDetails.IdentificationFrontSideName
	formDetails.IdentificationBackSide = formDetails.IdentificationBackSideName

	_, err = userIdentificationsCollection.InsertOne(ctx, formDetails.UserIdentifications)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Stored UserIdentifications in MondoDB")

	body, error := ParseTemplate("./module/templates/lead_new_email_admin.html", formDetails)

	if error != nil {
		log.Fatal(error)
		fmt.Println("Error in parsing template HTML")
	} else {

		fmt.Println("Template found, sending email now..")
	}

	adminEmail := os.Getenv("ADMIN_EMAIL_ADDRESS")
	
	email.SendEmail(adminEmail, "Equeduct - New Lead", body)
}

func InsertMongoContactUs(formDetails data.ContactUsData) {

	fmt.Println("ContactUs Form data MongoDB Started ...")

	hostName := os.Getenv("MONGO_DB_HOST")
	dbName := os.Getenv("MONGO_DB_NAME")
	user := os.Getenv("MONGO_USER")
	pass := os.Getenv("MONGO_PASS")

	if dbName == "" || user == "" || pass == "" {
		fmt.Println("mongo credentials not set")
		return
	}
	uri := `mongodb+srv://` + user + `:` + pass + `@` + hostName +`/` + dbName + `?retryWrites=true&w=majority`

	fmt.Println("Trying to connect mongodb server...")

	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	
	if err != nil {
		fmt.Println("Could Not connect to mongo server")
		log.Fatal(err)
	}

	ctx := context.Background()
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Starting mongodb data saving...")

	// connecting to DB and collections
	db := client.Database("equeduct")
	contactUsCollection := db.Collection("tbl_contact_us_queries")
	
	formDetails.CreatedTime = time.Now()
	result, err := contactUsCollection.InsertOne(ctx, formDetails)

	//objectID := fmt.Sprintf("%v", result.InsertedID)
	objectID := result.InsertedID.(primitive.ObjectID).Hex()

	if err != nil {
		log.Fatal(err)
		fmt.Println("Error in storing MondoDB")
	}

	fmt.Println("Stored ContactUs in MondoDB as: " + objectID)

	templateData := struct {
		Subject 		string
		Email  			string
		Description  	string
		QueryForType  	string
	}{
		Subject: 		formDetails.Subject,
		Email:  		formDetails.Email,
		Description:  	formDetails.Description,
		QueryForType:  	formDetails.QueryForType,
	}

	body, error := ParseTemplate("./module/templates/contact_us_new_email_admin.html", templateData)

	if error != nil {
		log.Fatal(error)
		fmt.Println("Error in parsing template HTML")
	} else {

		fmt.Println("Template found, sending email now..")
	}
	
	adminEmail := os.Getenv("ADMIN_EMAIL_ADDRESS")
	email.SendEmail(adminEmail, "Equeduct - New ContactUs Query", body)
}

func ParseTemplate(templateFileName string, data interface{}) (body string, err error) {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return "", err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return "", err
	}
	body = buf.String()
	return body, nil
}
